#ifndef _matrix_H_
#define _matrix_H_
#include <malloc.h>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "rcmatrix.h"

class CMatrix
{

		rcmatrix *data;

	public:

		class IndexOutOfRange
		{
		};
		class WrongDim
		{
		};
		class bad_alloc
		{
		};

		class cref1;
		class cref2;

		double read(int i, int j) const;
		CMatrix(int, int, double=0.0, double=0.0);
		CMatrix(const CMatrix&);
		CMatrix(fstream&);
		cref1 operator [](int i);
		void write(int, int, double);
		CMatrix& operator=(const CMatrix &temp);
		~CMatrix();



		friend ostream & operator <<(ostream & s, const CMatrix & matrix);
		friend CMatrix operator*(const CMatrix&, const CMatrix&);
};



class CMatrix::cref2
{

		CMatrix& s;
		int i;
		int j;
	public:
		cref2(CMatrix& ss, int ii, int jj) :
				s(ss), i(ii), j(jj)
		{
		}
		;

		cref2& operator=(double x)
		{
			s.write(i, j, x);
			return *this;
		}
		;

		operator double()
		{
			return s.read(i, j);
		}

};

class CMatrix::cref1
{
		friend class CMatrix;
		CMatrix& s;
		int i;

	public:
		cref2 operator [](int j)
		{
			if (j > s.data->cols)
				throw CMatrix::IndexOutOfRange();

			return cref2(s, i, j);
		};


		cref1(CMatrix& ss, int ii) :
				s(ss), i(ii)
		{
		}
		;

};





inline CMatrix operator *(const CMatrix& temp_matrix1, const CMatrix& temp_matrix2)
{
	if (temp_matrix1.data->cols != temp_matrix2.data->rows)
		throw CMatrix::WrongDim();
	else
	{

		CMatrix buff(temp_matrix1.data->rows, temp_matrix2.data->cols);

		for (int k = 0; k < temp_matrix1.data->rows; k++)
		{

			for (int l = 0; l < temp_matrix2.data->cols; l++)
			{

				for (int m = 0; m < temp_matrix1.data->cols; m++)
				{
					buff.data->matrix[k][l] += temp_matrix1.data->matrix[k][m]
							* temp_matrix2.data->matrix[m][l];
				}

			}

		}

		return buff;
	}
}

#endif
