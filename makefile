a.out: main.o matrix.o rcmatrix.o
	g++ main.o matrix.o rcmatrix.o
main.o: main.cpp matrix.o rcmatrix.o
	g++ -c -Wall -pedantic main.cpp
matrix.o:  matrix.h matrix.cpp rcmatrix.o
	g++ -c -Wall -pedantic matrix.cpp
rcmatrix.o: rcmatrix.h rcmatrix.cpp
	g++ -c -Wall -pedantic rcmatrix.cpp
