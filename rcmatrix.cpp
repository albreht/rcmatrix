#include "rcmatrix.h"
#include <iostream>
using namespace std;

double** rcmatrix::loggedAlloc(int rows, int cols)
{
	int i = 0;
	double **matrix = new double*[rows];

	try
	{

		for (i = 0; i < rows; i++)
		{
			matrix[i] = new double[cols];
		}

		return matrix;

	}
	catch (bad_alloc&)
	{

		for (int j = i - 1; j >= 0; j--)
		{
			delete[] matrix[j];
		}

		delete[] matrix;

		throw bad_alloc();
	}
}

rcmatrix::rcmatrix(int Rows, int Cols, double Dim, double Rest) :
		rows(Rows), cols(Cols)
{
	n = 1;
	matrix = loggedAlloc(Rows, Cols);
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (i == j)
				matrix[i][j] = Dim;
			else
				matrix[i][j] = Rest;
		}
	}

};

rcmatrix::rcmatrix(const rcmatrix& temp): rows(temp.rows), cols(temp.cols)
{
	int k = 0;
	int l = 0;
	n=1;
	matrix = loggedAlloc(temp.rows,temp.cols);
	for(k = 0; k < rows; k++)
	{
		for(l = 0; l < cols; l++)
		{
			matrix[k][l] = temp.matrix[k][l];
		}
	}
}

rcmatrix::rcmatrix(fstream & f)
{
	n=1;
	f>>rows;
	f>>cols;
	cout<<rows<<cols<<endl;

	matrix=loggedAlloc(rows,cols);

	for(int i=0;i<rows;i++)
	{
		for(int j=0;j<cols;j++)
		{

			f>>matrix[i][j];
		}
	}

}

rcmatrix* rcmatrix::detach()											// Detach
{
	if (n == 1)
		return this;

	rcmatrix* t = new rcmatrix(*this);
	n--;
	return t;
}

rcmatrix::~rcmatrix()
{
	int k = 0;
	for(k=0; k<rows; k++)
	{
		delete [] matrix[k];
	}

	delete [] matrix;
}


