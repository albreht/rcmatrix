#ifndef RCMATRIX_H_
#define RCMATRIX_H_

#include <fstream>

struct rcmatrix
{
		int rows;
		int cols;
		int n;
		double **matrix;

		double** loggedAlloc(int, int);
		rcmatrix(int, int, double, double);
		rcmatrix(const rcmatrix&);
		rcmatrix(std::fstream &);
		rcmatrix* detach();
		~rcmatrix();
};
#endif /* RCMATRIX_H_ */
