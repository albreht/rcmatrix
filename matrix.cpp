/*
 * matrix.cpp
 *
 *  Created on: 17.12.2017
 *      Author: test
 */
#include "matrix.h"

CMatrix::CMatrix(int rows, int cols, double dim, double rest)
{
	data = new rcmatrix(rows, cols, dim, rest);
}

CMatrix& CMatrix::operator=(const CMatrix &temp)
{
	temp.data->n++;
	if (--data->n == 0)
		delete data;

	data = temp.data;
	return *this;
}

CMatrix::CMatrix(const CMatrix& temp)
{
	temp.data->n++;
	data = temp.data;
}

CMatrix::CMatrix(fstream & f)
{
	data = new rcmatrix(f);
}

void CMatrix::write(int i, int j, double x)
{
	data = data->detach();
	data->matrix[i][j] = x;
}

CMatrix::~CMatrix()
{
	if (--data->n == 0)
		delete data;
}

double CMatrix::read(int i, int j) const
{
	return data->matrix[i][j];
}

CMatrix::cref1 CMatrix::operator[](int i)
{
	if (i > data->rows)
		throw IndexOutOfRange();
	return cref1(*this, i);
}
;

ostream & operator <<(ostream & output, const CMatrix & matrix)
{
	output << endl;
	output << "[ ";
	output << fixed << setprecision(1);

	for (int i = 0; i < matrix.data->rows; i++)
	{
		for (int j = 0; j < matrix.data->cols; j++)
		{
			output << matrix.read(i, j) << "  ";
		}
		if (i < matrix.data->rows - 1)
			output << "\n  ";
	}

	output << "]";
	return output;
}
;
