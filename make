a.out: glowny.o matrix.o
    g++ matrix.o rcmatrix.o

matrix.o: matrix.cpp matirx.hh
    g++ -c -Wall -pedantic matrix.cpp

rcmatrix.o: rcmatrix.hh rcmatrix.cpp
    g++ -c -Wall -pedantic rcmatrix.cpp
